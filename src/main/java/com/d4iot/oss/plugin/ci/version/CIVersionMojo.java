package com.d4iot.oss.plugin.ci.version;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.maven.RepositoryUtils;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.eclipse.aether.impl.VersionRangeResolver;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.resolution.VersionRangeResult;
import org.eclipse.aether.util.artifact.DefaultArtifactTypeRegistry;
import org.eclipse.jgit.api.errors.GitAPIException;

import com.d4iot.oss.plugin.ci.version.scm.git.GitUtils;
import com.d4iot.oss.plugin.ci.version.util.Utils;
import com.d4iot.oss.plugin.ci.version.util.VersionReport;

@Mojo(name = "ci-version", requiresProject = true,
        requiresDirectInvocation = true, aggregator = true,
        threadSafe = true, executionStrategy = "once-per-session")

public class CIVersionMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}", required = true,
            readonly = true)
    protected MavenProject project;

    @Parameter(defaultValue = "${session}", readonly = true,
            required = true)
    private MavenSession session;

    @Parameter(property = "tagSCM", defaultValue = "true")
    private boolean tagSCM;

    @Parameter(property = "branchAndTagSCM", defaultValue = "true")
    private boolean branchTagSCM;
    @Parameter(property = "versionRangeCleanup", defaultValue = "true")
    private boolean vrCleanup;

    @Component
    protected VersionRangeResolver versionReangeResolver;

    private final Map<String, com.d4iot.oss.plugin.ci.version.util.VersionReport.Version> versionMap =
            new HashMap<>();

    @Override
    public void execute()
            throws MojoExecutionException, MojoFailureException {

        validatePoms();
        // Do we need to do a backup here - Not really
        try {
            final String origProjVersion =
                    project.getOriginalModel().getVersion();
            final Version pomVersion =
                    new Version('s', project.getVersion());
            final GitUtils gitUtils = new GitUtils();
            final Version curVersion =
                    gitUtils.getHighestVersion(pomVersion);
            final Version nextVersion = curVersion.nextVersion();
            final String newVersion = nextVersion.tagName(' ');
            if (tagSCM) {
                getLog().info("Applyting tag to source branch");
                gitUtils.tagSource(nextVersion);
            }
            if (branchTagSCM) {
                getLog().info("Branching SCM");
                gitUtils.branch(nextVersion);
            }
            resolveRanges();
            updateVersions(newVersion);

            new VersionReport(
                    new com.d4iot.oss.plugin.ci.version.util.VersionReport.Version(
                            project.getOriginalModel().getGroupId(),
                            project.getOriginalModel().getArtifactId(),
                            origProjVersion, newVersion),
                    gitUtils.getCommitId(), versionMap.values())
                            .write(new File(
                                    project.getBuild().getDirectory(),
                                    "range-resolve-report.json"));
            Utils.writePoms(session, getLog());
            if (branchTagSCM) {
                getLog().info("Commit and tag current SCM Branch");
                gitUtils.commitAndTagBranch(nextVersion);
            }
        } catch (final IOException | VersionRangeResolutionException
                | GitAPIException e) {
            throw new MojoFailureException(e.getMessage(), e);
        }
    }

    /**
     * Lets get rid of snapshots and those pesky non-numeric build versions
     *
     * @param result
     *            result of range resolution
     */
    private void cleanupRanges(
            final VersionRangeResult result) {
        // First cleanup lower non-numeric build versions
        final List<org.eclipse.aether.version.Version> versionsCleanup =
                new ArrayList<>(result.getVersions());
        versionsCleanup.remove(result.getHighestVersion());
        for (final org.eclipse.aether.version.Version dVer : versionsCleanup) {
            final String[] split = dVer.toString().split("-");
            if ((split.length < 2) || ((split.length > 1)
                    && StringUtils.isNumeric(split[1]))) {
                continue;
            }
            getLog().warn("   Discarding " + dVer);
            result.getVersions().remove(dVer);
        }
        while (result.getVersions().size() > 1) {
            final org.eclipse.aether.version.Version dVer =
                    result.getHighestVersion();
            final String[] split = dVer.toString().split("-");
            if ((split.length < 2) || ((split.length > 1)
                    && StringUtils.isNumeric(split[1]))) {
                break;
            }
            getLog().warn("   Discarding " + dVer);
            result.getVersions().remove(dVer);
        }

    }

    private void resolveRanges() throws VersionRangeResolutionException {

        final List<RemoteRepository> repoList = new ArrayList<>();
        final DefaultArtifactTypeRegistry typeRegistry =
                new DefaultArtifactTypeRegistry();
        // Get remote Repositories
        for (final Object rep : project.getRemoteArtifactRepositories()) {
            repoList.add(RepositoryUtils.toRepo((ArtifactRepository) rep));
        }
        final Map<String, com.d4iot.oss.plugin.ci.version.util.VersionReport.Version> artifactMap =
                new HashMap<>();

        for (final MavenProject curProject : session.getAllProjects()) {
            getLog().info("Resloving Ranges for project : "
                    + curProject.getName());
            if (curProject.isExecutionRoot() && (curProject
                    .getOriginalModel().getParent() != null)) {
                final Dependency dependency = new Dependency();
                dependency.setGroupId(curProject.getOriginalModel()
                        .getParent().getGroupId());
                dependency.setArtifactId(curProject.getOriginalModel()
                        .getParent().getArtifactId());
                dependency.setVersion(curProject.getOriginalModel()
                        .getParent().getVersion());
                getLog().info("  Project Parent "
                        + dependency.getManagementKey() + ":"
                        + dependency.getVersion());
                resolveRanges(Collections.singletonList(dependency),
                              artifactMap, repoList, typeRegistry);
            }
            if ((curProject.getModel().getDependencyManagement() != null)
                    && (curProject.getModel().getDependencyManagement()
                            .getDependencies() != null)) {
                resolveRanges(curProject.getModel()
                        .getDependencyManagement().getDependencies(),
                              artifactMap, repoList, typeRegistry);
            }
            if (curProject.getModel().getDependencies() != null) {
                resolveRanges(curProject.getModel().getDependencies(),
                              artifactMap, repoList, typeRegistry);
            }
        }
        for (final MavenProject curProject : session.getAllProjects()) {
            if (curProject.isExecutionRoot() && (curProject
                    .getOriginalModel().getParent() != null)) {
                final Dependency dependency = new Dependency();
                dependency.setGroupId(curProject.getOriginalModel()
                        .getParent().getGroupId());
                dependency.setArtifactId(curProject.getOriginalModel()
                        .getParent().getArtifactId());
                dependency.setVersion(curProject.getOriginalModel()
                        .getParent().getVersion());
                updateRanges(Collections.singletonList(dependency),
                             artifactMap);
                curProject.getOriginalModel().getParent()
                        .setVersion(dependency.getVersion());
            }
            if (curProject.getOriginalModel().getDependencies() != null) {
                updateRanges(curProject.getOriginalModel()
                        .getDependencies(), artifactMap);
            }
        }

    }

    private void resolveRanges(
            final List<Dependency> dependencies,
            final Map<String, VersionReport.Version> artifactMap,
            final List<RemoteRepository> repoList,
            final DefaultArtifactTypeRegistry typeRegistry)
            throws VersionRangeResolutionException {
        for (final Dependency dependency : dependencies) {
            final String version = dependency.getVersion();
            if (StringUtils.isAllBlank(version)
                    || !version.contains(",")) {
                continue;
            }
            final VersionRangeRequest request = new VersionRangeRequest(
                    RepositoryUtils.toDependency(dependency, typeRegistry)
                            .getArtifact(),
                    repoList, null);
            final VersionRangeResult result = versionReangeResolver
                    .resolveVersionRange(session.getRepositorySession(),
                                         request);
            final StringBuilder verList = new StringBuilder();
            final int elemCnt = result.getVersions().size();
            final int elemStart = elemCnt > 10 ? elemCnt - 10 : 0;
            if (elemStart > 0) {
                verList.append("...... ");
            }
            for (final org.eclipse.aether.version.Version ver : result
                    .getVersions().subList(elemStart, elemCnt)) {
                verList.append(" ").append(ver);
            }

            getLog().info("  Ranges for " + dependency.getManagementKey()
                    + verList);
            if (vrCleanup) {
                cleanupRanges(result);
            }
            artifactMap
                    .put(dependency.getManagementKey(),
                         new VersionReport.Version(dependency.getGroupId(),
                                 dependency.getArtifactId(),
                                 dependency.getVersion(),
                                 result.getHighestVersion().toString()));
        }

    }

    private void updateRanges(
            final List<Dependency> dependencies,
            final Map<String, VersionReport.Version> artifactMap) {
        for (final Dependency dependency : dependencies) {
            final VersionReport.Version versionVer =
                    artifactMap.get(dependency.getManagementKey());
            if (versionVer == null) {
                continue;
            }
            final String oldVer = versionVer.getVersionOld() + "("
                    + dependency.getVersion() + ")";
            getLog().info("  Range Version Documenting"
                    + dependency.getManagementKey() + ":" + oldVer
                    + "....to...." + versionVer.getVersionNew());
            versionMap
                    .put(dependency.getManagementKey(),
                         new com.d4iot.oss.plugin.ci.version.util.VersionReport.Version(
                                 dependency.getGroupId(),
                                 dependency.getArtifactId(),
                                 versionVer.getVersionOld(),
                                 versionVer.getVersionNew()));
            getLog().info("  Range Version Updating "
                    + dependency.getManagementKey() + ":" + oldVer
                    + "....to...." + versionVer.getVersionNew());
            dependency.setVersion(versionVer.getVersionNew());
        }
    }

    private void updateVersions(
            final String newVersion) {

        getLog().info("Setting Main Project Version to " + newVersion);
        final Model mainProject = project.getOriginalModel();
        mainProject.setVersion(newVersion);

        for (final MavenProject moduleProject : session.getProjects()) {
            if (moduleProject.isExecutionRoot()) {
                continue;
            }
            getLog().info("   Setting Module " + moduleProject.getName()
                    + " parent version to " + newVersion);
            final Model model = moduleProject.getOriginalModel();
            model.getParent().setVersion(newVersion);
        }
    }

    /**
     * Validate that the poms are setup the way we expect them.
     */
    private void validatePoms() {

        final Model mainProject = project.getOriginalModel();
        getLog().info("Validating Main Project " + mainProject.getName());
        final String groupId = mainProject.getGroupId();
        final String artifactId = mainProject.getArtifactId();
        final String version = mainProject.getVersion();

        Validate.notBlank(groupId, "Main POM Group Id is not specified");
        Validate.notBlank(artifactId,
                          "Main POM Artifact Id is not specified");
        Validate.notBlank(version, "Main POM Version is not specified");
        final File pomFile = mainProject.getPomFile();
        Validate.notNull(pomFile, "Cannot get Main pom file");
        for (final MavenProject moduleProject : session.getProjects()) {
            if (moduleProject.isExecutionRoot()) {
                continue;
            }
            getLog().info("   Validating Module "
                    + moduleProject.getName());
            final Model model = moduleProject.getOriginalModel();
            Validate.isTrue(model.getVersion() == null,
                            "Module POM version MUST NOT be specified");
            Validate.isTrue(model.getGroupId() == null,
                            "Module POM Group Id MUST NOT be specified");
            Validate.notBlank(model.getArtifactId(),
                              "Module POM Artifact Id MUST  be specified");
            Validate.isTrue(groupId
                    .contentEquals(model.getParent().getGroupId()),
                            "Module POM Parent Group Id MUST match main pom");
            Validate.isTrue(artifactId
                    .contentEquals(model.getParent().getArtifactId()),
                            "Module POM Parent Artifact Id MUST match main pom");
            Validate.isTrue(version
                    .contentEquals(model.getParent().getVersion()),
                            "Module POM Parent version MUST match main pom");
            final File file = model.getPomFile();
            Validate.notNull(file, "Cannot get Module pom file");
        }
    }

}

package com.d4iot.oss.plugin.ci.version;

import java.util.Calendar;
import java.util.regex.Pattern;

import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

public class Version extends DefaultArtifactVersion {
    public static final char PREFIX_SOURCE = 's';
    public static final char PREFIX_VERSION = 'v';
    public static final Pattern SOURCE_VERSION_PATTERN =
            Pattern.compile("s[0-9]+\\.[0-9]+\\.[0-9]+-[0-9]+");
    private final char prefix;
    private Calendar versionDate = Calendar.getInstance();

    public Version(final char pPrefix, final String pVersion) {
        super(pVersion);
        prefix = pPrefix;
    }

    /**
     * Return version that is same major/minor and the higher of the two
     * version.
     *
     * @param pVersion
     *            version to compare against
     * @return version
     */
    public Version getHigher(
            final Version pVersion) {
        if (pVersion == null) {
            return this;
        }
        if ((pVersion.getMajorVersion() != getMajorVersion())
                || (pVersion.getMinorVersion() != getMinorVersion())) {
            return this;
        }
        if (pVersion.getIncrementalVersion() > getIncrementalVersion()) {
            return pVersion;
        }
        if ((pVersion.getIncrementalVersion() == getIncrementalVersion())
                && (pVersion.getBuildNumber() > getBuildNumber())) {
            return pVersion;
        }
        return this;
    }

    public char getPrefix() {
        return prefix;
    }

    public Calendar getVersionDate() {
        return versionDate;
    }

    public boolean isSource() {
        return prefix == PREFIX_SOURCE;
    }

    public boolean isVersion() {
        return prefix == PREFIX_VERSION;
    }

    /**
     * Get the next version with Wednesday (Hump Day) as cut-off date.
     *
     * @return version
     */
    public Version nextVersion() {
        final Calendar curCalendar = Calendar.getInstance();
        curCalendar.set(Calendar.HOUR_OF_DAY, 0);
        curCalendar.set(Calendar.MINUTE, 0);
        curCalendar.set(Calendar.SECOND, 0);
        curCalendar.set(Calendar.MILLISECOND, 0);
        int daysToHumpDay =
                curCalendar.get(Calendar.DAY_OF_WEEK) - Calendar.WEDNESDAY;
        if (daysToHumpDay < 0) {
            daysToHumpDay += 7;
        }
        curCalendar.add(Calendar.DATE, -1 * daysToHumpDay);
        int buildNumber = getBuildNumber();
        int incrementalNumber = getIncrementalVersion();
        if (versionDate.before(curCalendar)) {
            incrementalNumber++;
            buildNumber = 0;
        }
        final char sep = '.';
        return new Version(prefix,
                new StringBuilder().append(getMajorVersion()).append(sep)
                        .append(getMinorVersion()).append(sep)
                        .append(incrementalNumber).append('-')
                        .append(buildNumber + 1).toString().trim());

    }

    public void setVersionDate(
            final Calendar versionDate) {
        this.versionDate = versionDate;
    }

    public String tagName() {
        return tagName(prefix);
    }

    public String tagName(
            final char pPrefix) {

        final StringBuilder tag = new StringBuilder();
        final char sep = '.';
        return tag.append(pPrefix).append(getMajorVersion()).append(sep)
                .append(getMinorVersion()).append(sep)
                .append(getIncrementalVersion()).append('-')
                .append(getBuildNumber()).toString().trim();
    }

    public static Version getSourceTagVersion(
            final String pTagVersion) {
        if (!SOURCE_VERSION_PATTERN.matcher(pTagVersion).matches()) {
            return null;
        }
        return new Version(pTagVersion.charAt(0),
                pTagVersion.substring(1));
    }

    public static boolean isTagSource(
            final String pVersion) {
        return pVersion.charAt(0) == PREFIX_SOURCE;
    }

    public static boolean isTagVersion(
            final String pVersion) {
        return pVersion.charAt(0) == PREFIX_VERSION;
    }
}

package com.d4iot.oss.plugin.ci.version.scm.git;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import org.apache.commons.lang3.Validate;
import org.eclipse.jgit.api.CreateBranchCommand.SetupUpstreamMode;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevTag;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import com.d4iot.oss.plugin.ci.version.Version;

public class GitUtils {

    private final Repository gitRepository;
    private final Ref refHead;
    private final Git git;

    public GitUtils() throws IOException {
        this("./.git");
    }

    public GitUtils(final String pGitDirectory) throws IOException {
        final FileRepositoryBuilder repositoryBuilder =
                new FileRepositoryBuilder();
        gitRepository = repositoryBuilder
                .setGitDir(new File(pGitDirectory)).readEnvironment()
                .findGitDir().setMustExist(true).build();
        refHead = gitRepository.findRef("HEAD");
        git = new Git(gitRepository);
    }

    public void branch(
            final Version ver) throws GitAPIException {
        final String branchName = "ci-version/" + ver.tagName('v');
        git.checkout().setCreateBranch(true).setName(branchName)
                .setUpstreamMode(SetupUpstreamMode.TRACK).call();
    }

    public void commitAndTagBranch(
            final Version ver) throws GitAPIException {
        git.commit().setAll(true)
                .setMessage("ci-version-plugin auto commit").call();
        final String tagName = ver.tagName('v');
        git.tag().setName(tagName).call();
    }

    public String getCommitId() {
        return refHead.getObjectId().getName();
    }

    public Version getHighestVersion(
            final Version pomVersion) throws IOException {
        Version retVersion = pomVersion;
        final char sep = '.';
        final String prefix = new StringBuilder(Constants.R_TAGS)
                .append(Version.PREFIX_SOURCE)
                .append(pomVersion.getMajorVersion()).append(sep)
                .append(pomVersion.getMinorVersion()).append(sep)
                .toString();
        for (final Ref tagRef : gitRepository.getRefDatabase()
                .getRefsByPrefix(prefix)) {
            retVersion = retVersion
                    .getHigher(Version.getSourceTagVersion(tagRef.getName()
                            .substring(Constants.R_TAGS.length())));
        }

        if (retVersion == pomVersion) {
            return pomVersion;
        }
        // Lets get date of tag
        try (final RevWalk revWalk = new RevWalk(gitRepository)) {
            final String tagName = retVersion.tagName();
            final Ref tagRef =
                    gitRepository.findRef(Constants.R_TAGS + tagName);
            Validate.notNull(tagRef, "Tag Not found " + tagName);
            PersonIdent personIdent;
            RevTag revTag = null;
            try {
                revTag = revWalk.parseTag(tagRef.getObjectId());
                personIdent = revTag.getTaggerIdent();
            } catch (final IncorrectObjectTypeException exc) {
                personIdent = revWalk.parseCommit(tagRef.getObjectId())
                        .getAuthorIdent();
            }
            final Calendar calendar =
                    Calendar.getInstance(personIdent.getTimeZone());
            calendar.setTime(personIdent.getWhen());
            retVersion.setVersionDate(calendar);
        }

        return retVersion;

    }

    public void tagSource(
            final Version ver) throws GitAPIException {
        final String tagName = ver.tagName('s');
        git.tag().setName(tagName).call();
    }

}

package com.d4iot.oss.plugin.ci.version.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.DefaultModelWriter;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;

public class Utils {

    private Utils() {
        // Do Nothing
    }

    public static void writePoms(
            final MavenSession session,
            final Log log) throws IOException {
        final Map<String, Object> options = new HashMap<>();
        final DefaultModelWriter modelWriter = new DefaultModelWriter();

        for (final MavenProject moduleProject : session.getProjects()) {
            final Model model = moduleProject.getOriginalModel();
            log.info("Saving " + model.getArtifactId() + " to "
                    + model.getPomFile().getCanonicalPath());
            modelWriter.write(model.getPomFile(), options, model);
        }

    }
}

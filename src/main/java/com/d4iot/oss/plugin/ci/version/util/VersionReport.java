package com.d4iot.oss.plugin.ci.version.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.io.Files;
import com.jsoniter.output.JsonStream;

public class VersionReport {
    public static class Version {
        private String groupId;
        private String artifactId;
        private String versionOld;
        private String versionNew;

        public Version(final String pGroupId, final String pArtifactId,
                final String pVersionOld, final String pVersionNew) {
            groupId = pGroupId;
            artifactId = pArtifactId;
            versionOld = pVersionOld;
            versionNew = pVersionNew;
        }

        public String getArtifactId() {
            return artifactId;
        }

        public String getGroupId() {
            return groupId;
        }

        public String getVersionNew() {
            return versionNew;
        }

        public String getVersionOld() {
            return versionOld;
        }

        public void setArtifactId(
                final String artifactId) {
            this.artifactId = artifactId;
        }

        public void setGroupId(
                final String groupId) {
            this.groupId = groupId;
        }

        public void setVersionNew(
                final String versionNew) {
            this.versionNew = versionNew;
        }

        public void setVersionOld(
                final String versionOld) {
            this.versionOld = versionOld;
        }

    }

    private Version project;
    private String scmId;
    private List<Version> dependencies = new ArrayList<>();

    public VersionReport(final Version projectVersion, final String pScmId,
            final Collection<Version> rangeDependencies) {
        project = projectVersion;
        scmId = pScmId;
        dependencies.addAll(rangeDependencies);
    }

    public List<Version> getDependencies() {
        return dependencies;
    }

    public Version getProject() {
        return project;
    }

    public String getScmId() {
        return scmId;
    }

    public void setDependencies(
            final List<Version> dependencies) {
        this.dependencies = dependencies;
    }

    public void setProject(
            final Version project) {
        this.project = project;
    }

    public void setScmId(
            final String scmId) {
        this.scmId = scmId;
    }

    public void write(
            final File file) throws IOException {
        file.getParentFile().mkdirs();
        Files.write(JsonStream.serialize(this).getBytes(), file);

    }

}

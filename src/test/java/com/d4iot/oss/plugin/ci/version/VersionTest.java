package com.d4iot.oss.plugin.ci.version;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.util.Calendar;

import org.junit.jupiter.api.Test;

import mockit.Expectations;

public class VersionTest {

    @Test
    void testHigher() {
        final Version ver1 = new Version('v', "0.0.0-1");
        final Version ver2 = new Version('s', "0.0.0-10");
        final Version ver2s = new Version('v', "0.0.0-10");
        final Version ver3 = new Version('v', "0.0.0-100");
        final Version ver4 = new Version('v', "0.0.1-1");
        final Version ver5 = new Version('v', "1.1.1-1");
        final Version ver6 = new Version('v', "1.0.1-1");
        final Version ver7 = new Version('s', "1.0.3-9");
        final Version ver8 = new Version('s', "1.0.16-4");
        assertTrue(ver8.getHigher(ver7) == ver8);
        assertTrue(ver7.getHigher(ver8) == ver8);
        assertTrue(ver2.getHigher(ver1) == ver2);
        assertTrue(ver2.getHigher(ver2s) == ver2);
        assertTrue(ver2s.getHigher(ver2) == ver2s);
        assertTrue(ver2.getHigher(ver3) == ver3);
        assertTrue(ver4.getHigher(ver3) == ver4);
        assertTrue(ver5.getHigher(ver3) == ver5);
        assertTrue(ver5.getHigher(ver6) == ver5);
        assertTrue(ver3.getHigher(ver5) == ver3);
        assertTrue(ver3.getHigher(null) == ver3);
    }

    @Test
    void testIncr() throws ParseException {
        assertEquals("1.0.0-1", new Version(' ', "1.0.0-SNAPSHOT")
                .nextVersion().tagName());
        assertEquals("1.0.0-1",
                     new Version(' ', "1.0.0-0").nextVersion().tagName());
        final Version ver = new Version('s', "0.0.0-10");
        final Calendar cal = Calendar.getInstance();
        cal.set(2019, 1, 1);
        ver.setVersionDate(cal);
        assertEquals("s0.0.1-1", ver.nextVersion().tagName());
    }

    @Test
    void testIncrMocked() throws ParseException {
        final Version ver = new Version('s', "0.0.0-10");
        final Calendar cal1 = Calendar.getInstance();
        cal1.set(2019, 0, 1);
        cal1.getTime();
        final Calendar cal2 = Calendar.getInstance();
        cal2.set(2019, 0, 2); // Wednesday
        cal2.getTime();
        final Calendar cal3 = Calendar.getInstance();
        cal3.set(2019, 0, 3);
        cal3.getTime();
        new Expectations(Calendar.class) {
            {
                Calendar.getInstance();
                result = cal1;
            }
        };

        ver.setVersionDate(cal1);
        assertEquals("s0.0.0-11", ver.nextVersion().tagName());
        new Expectations(Calendar.class) {
            {
                Calendar.getInstance();
                result = cal3;
            }
        };
        ver.setVersionDate(cal1);
        assertEquals("s0.0.1-1", ver.nextVersion().tagName());
        ver.setVersionDate(cal2);
        assertEquals("s0.0.0-11", ver.nextVersion().tagName());
        assertTrue(ver.getVersionDate() == cal2);
    }

    @Test
    void testMisc() {
        assertNull(Version.getSourceTagVersion("v0.0.0-1"));
        assertEquals("s0.0.0-1",
                     Version.getSourceTagVersion("s0.0.0-1").tagName());
        assertTrue(Version.isTagSource("s0.0.0-1"));
        assertFalse(Version.isTagSource("v0.0.0-1"));
        assertTrue(Version.isTagVersion("v0.0.0-1"));
        assertFalse(Version.isTagVersion("s0.0.0-1"));
        final Version ver1 = new Version('v', "0.0.0-1");
        final Version ver2 = new Version('s', "0.0.0-10");
        final Version ver3 = new Version('x', "0.0.0-10");
        assertEquals('v', ver1.getPrefix());
        assertEquals('s', ver2.getPrefix());
        assertTrue(ver2.isSource());
        assertFalse(ver1.isSource());
        assertTrue(ver1.isVersion());
        assertFalse(ver2.isVersion());
        assertFalse(ver3.isVersion());
        assertFalse(ver3.isSource());

    }

    @Test
    void testVer() {
        assertEquals("v1.0.0-0",
                     new Version('s', "1.0.0-SNAPSHOT").tagName('v'));
        assertEquals("s1.0.0-0",
                     new Version('s', "1.0.0-SNAPSHOT").tagName());
        assertEquals("s0.0.0-0", new Version('s', "0.0.0-0").tagName());
    }
}

package com.d4iot.oss.plugin.ci.version.scm.git;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import com.d4iot.oss.plugin.ci.version.Version;

public class GitUtilsTest {

    @Test
    void testMisc() throws IOException {
        final GitUtils gitUtils = new GitUtils();
        assertNotNull(gitUtils.getCommitId());
    }

    @Test
    void testVersions() throws IOException {
        final GitUtils gitUtils = new GitUtils();
        Version pomVersion = new Version(' ', "0.0.0-SNAPSHOT");
        Version ver = gitUtils.getHighestVersion(pomVersion);
        assertEquals("s0.0.0-2", ver.tagName());
        pomVersion = new Version(' ', "0.1.1-SNAPSHOT");
        ver = gitUtils.getHighestVersion(pomVersion);
        assertEquals("s0.1.1-1", ver.tagName());
        pomVersion = new Version('s', "0.1.2-SNAPSHOT");
        ver = gitUtils.getHighestVersion(pomVersion);
        assertEquals("s0.1.2-0", ver.tagName());
        pomVersion = new Version('s', "0.2.0-SNAPSHOT");
        ver = gitUtils.getHighestVersion(pomVersion);
        assertEquals("s0.2.0-0", ver.tagName());
    }
}
